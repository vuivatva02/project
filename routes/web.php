<?php

use App\Http\Controllers\CustomerController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/customer/{id}/{name}',[CustomerController::class, 'index']);
Route::get('/posts',[CustomerController::class, 'posts']);
Route::get('/hinh-chu-nhat/{dai}/{rong}',[CustomerController::class, 'hinh_chu_nhat']);
Route::post('/posts',[CustomerController::class, 'posts'])->name('Search');

// Route::match(['get','post'],'/add', [CustomerController::class, 'add_post'])->name('AddPost');
Route::match(['get','post'],'/add', [CustomerController::class, 'add_post'])->name('AddCustomer');
