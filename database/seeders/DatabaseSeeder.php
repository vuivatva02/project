<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        for ($i=0; $i <2 ; $i++) { 
            DB::table('posts')->insert(
                [
                    'Title'=>'Title',
                    'Content'=>'Content',
                    'Publish Date' => '2023/06/30',
                    'Author'=> $i,
                    'Image'=> 'aaaaaaaaaaaaaaa',
                 ]
                
            );
        }
            
    }
}
