<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CustomerController extends Controller
{
    public function index($id,$name){
        dd("Xin chao $name, Co Id la $id");
    }

    public function hinh_chu_nhat($dai,$rong){
        $chu_vi = ($dai + $rong) * 2;
        $dien_tich = $dai * $rong;
        echo("Hinh chu nhat co Chieu dai la $dai, Chieu rong la $rong ");
        echo("Chu vi hinh chu nhat= $chu_vi ");
        echo("Dien tich hinh chu nhat= $dien_tich");
    }

    public function posts(Request $request){
        // $post = DB::table('posts')->count();
        // dd($post);

        // $post = DB::table('posts')->where('id', 2)
        //                         ->update([
        //                             'title' => 'Bui Quynh Ninh',
        //                             'author' => 3
        //                         ]);

        $post = DB::table('posts')->get();
        if($request){
            $post = DB::table('posts')->where('title', 'like', '%'. $request->search .'%')->get();
        }

        return view('test.index', compact('post'));

    }

    public function add_post(Request $request){
        $title = 'Add new User';

        if($request->isMethod('post')){
            $params = $request->post();
            unset($params['_token']);
            
            DB::table('users')->insert($params);
        }
        return view('test.add', compact('title'));
    }
    
}
