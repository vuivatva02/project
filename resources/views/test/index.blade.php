<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
</head>
<body>
    <h1>Hello World</h1>
    <table class="table table-bordered border-black">
        <thead>
                <th>ID</th>
                <th>Title</th>
                <th>Content</th>
                <th>Publish Date</th>
                <th>Author</th>
                <th>Image</th>
        </thead>
        <tbody>
            {{-- {{$post}} --}}
            @foreach ($post as $item)
                <tr>
                    <td>{{$item->id}}</td>
                    <td>{{$item->Title}}</td>
                    <td>{{$item->Content}}</td>
                    <td>{{$item->Publish_Date}}</td>
                    <td>{{$item->Author}}</td>
                    <td>{{$item->Image}}</td>
                </tr>
            @endforeach
        </tbody>        
    </table>

    <form action="{{route('Search')}}" method="">
        @csrf
        <input type="text" name="search" id="">
        <input type="submit" value="search" name="btn_search">
    </form>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous"></script>
</body>
</html>
{{-- <form action='{{ route("add_customer") }}' method="POST">
    <input type="text" placeholder="Name" name="name" ><br>
    <input type="text" placeholder="Age" name="age"><br>
    <input type="text" placeholder="Phone Number" name="phone_number" ><br>
    <input type="text" placeholder="Email" name="email" ><br>
    <input type="text" placeholder="Address" name="address" ><br>
    <input type="date" name="date_of_birth" ><br>
    <input type="radio" name="gender" value="0" >
    <label for="">Nam</label>
    <input type="radio" name="gender" value="1">
    <label for="">Nữ</label><br>
    <input type="submit" name="submit_add" value="Add">
</form> --}}
