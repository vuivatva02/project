<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ $title }}</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
</head>

<body>
    <form action='{{ route('AddCustomer') }}' method="POST">
        @csrf
        <input type="text" placeholder="Name" name="name"><br>
        <input type="number" placeholder="Phone Number" name="phone_number"><br>
        <input type="email" placeholder="Email" name="email"><br>
        <input type="password" placeholder="Password" name="password"><br>
        <input type="text" placeholder="Address" name="address"><br>
        <input type="date" name="date_of_birth"><br>
        <input type="radio" name="gender" value="0">
        <label for="">Nam</label>
        <input type="radio" name="gender" value="1">
        <label for="">Nữ</label><br>
        <button>Submit</button>
    </form>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
</body>

</html>
